package trivialv2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;
import static trivialv2.Main.playersList;
import static trivialv2.Main.questionList;

/**
 *
 * @author Eneko Rodríguez Gonzáñez <enekorodriguezgonzalez at gmail.com>
 */
public class Game {

    /**
     * Clear console
     */
    public static void clearConsole() {
        for (int i = 0; i < 50; i++) {
            System.out.println("");
        }
    }

    /**
     * Show line of - as separator
     */
    public static void showSeparator() {
        String separator = "";
        for (int i = 0; i < 20; i++) {
            separator += "-";
        }
        System.out.println(separator + "\n");
    }

    /**
     * Ask for name of new players and generate them
     */
    public static void makeNewPlayers() {
        Database.clearPlayers();
        playersList.clear();
        System.out.println("No hay jugadores guardados.");

        System.out.println("Introduce los nombres de los jugadores. Para salir introduce :q");

        boolean writingPlayers = true;
        
        do {
            writingPlayers = Player.generatePlayer();

        } while (playersList.isEmpty() || writingPlayers);

        Database.storePlayers();
    }

    /**
     *  Obtain random question form questionList
     * @return Random Question
     */
    public static Question getRandomQuestion() {
        Random rand = new Random();
        int randomNumber = rand.nextInt(questionList.size());

        return questionList.get(randomNumber);
    }

    /**
     * Generate a summary of scores of players, sort by score
     * @param showWinner If it's true then show the winner player
     */
    public static void showSummaryGame(boolean showWinner) {
        ArrayList<Player> playersSortByScore = playersList;

        Collections.sort(playersSortByScore, (Player p1, Player p2) -> {

            // Aqui esta el truco, ahora comparamos p2 con p1 y no al reves como antes
            return new Integer(p2.getScore()).compareTo(p1.getScore());

        });

        System.out.println("");

        for (Player player : playersSortByScore) {
            System.out.format("%d | %s", playersSortByScore.indexOf(player) + 1, player.showSummary());
        }

        System.out.println("");

        if (showWinner) {
            System.out.format("El ganador es %s, enhorabuena!!!!\n", playersSortByScore.get(0).getName());
        }
    }

    /**
     * Main process of game, make all rounds, inside ask all players one question, after each round show a summary, in final show winner
     */
    public static void game() {
        for (int actualRound = 0; actualRound < Config.getMaxRounds(); actualRound++) {
            showSeparator();
            System.out.format("\nRonda %d de %d\n", actualRound, Config.getMaxRounds());

            for (Player actualPlayer : playersList) {
                System.out.println("");
                System.out.format("Preguna para %s\n", actualPlayer.getName());

                Question questionRound = getRandomQuestion();

                if (questionRound.askQuestion()) {
                    actualPlayer.sumScore(questionRound.getScore());
                    Database.updateScore(actualPlayer);
                }

            }
            showSummaryGame(false);
            Database.storePlayers();

        }
    }
    
    /**
     * Show scores of the game and say the winner
     */
    public static void showWinner() {
        System.out.println("El juego ha acabado, las puntuaciones son las siguientes");

        showSummaryGame(true);
    }

    /**
     * Ask int and catch the exception while the number is invalid
     * @param message Message to show every ask
     * @return Int write by the user
     */
    public static int getInt(String message) {
        Scanner input = new Scanner(System.in);
        boolean writing = true;
        int result = 0;

        while (writing) {
            if (!message.equals("")) {
                System.out.println(message);
            }

            try {
                result = input.nextInt();
                writing = false;

            } catch (Exception e) {
                System.out.println("Número no válido");
                input.next();
            }
        }
        return result;
    }
    
    /**
     * Ask int, greater than argument, and catch the exception while the number is invalid
     * @param message Message to show every ask
     * @param minNumber Minimun number to be valid
     * @return Int write by the user
     */
    public static int getInt(String message, int minNumber) {
        Integer result;

        do {
            result = getInt(message);

            if (result < minNumber) {
                System.out.println("El número debe ser mayor que " + minNumber);
            }
        }
        while (result < minNumber);
        
        return result;
    }
    
    /**
     * Ask int, greater than minNumber and less than the maxNumber, and catch the exception while the number is invalid
     * @param message Message to show every ask
     * @param minNumber Minimun number to be valid
     * @param maxNumber Maximun number to be valid
     * @return Int write by the user
     */
    public static int getInt(String message, int minNumber, int maxNumber) {
        Integer result;

        do {
            result = getInt(message);

            if (result < minNumber || result > maxNumber) {
                System.out.println("Número no válido");
            }
        
        } while (result < minNumber || result > maxNumber);
        
        return result;
    }
}
