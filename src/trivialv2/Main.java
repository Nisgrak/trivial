package trivialv2;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import static trivialv2.Game.*;

/**
 *
 * @author Eneko Rodríguez Gonzáñez <enekorodriguezgonzalez at gmail.com>
 */
public class Main {

    public static ArrayList<Player> playersList = new ArrayList();
    public static ArrayList<Question> questionList = new ArrayList();

    /**
     * Entry point of proyect
     * @param args the command line arguments
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws SQLException, IOException, InterruptedException {
        Database.initialize();

        boolean inMenu = true;
        while (inMenu) {
            String menuText = "\n1. Empezar juego!!!\n"
                    + "2. Continuar juego anterior\n"
                    + "3. Configurar juego\n"
                    + "4. Salir";

            switch (getInt(menuText, 1, 4)) {
                case 1:
                    makeNewPlayers();
                case 2:
                    if (playersList.isEmpty()) {
                        makeNewPlayers();
                    }
                    game();
                    showWinner();
                    break;
                case 3:
                    Config.showConfig();
                    break;
                case 4:
                    inMenu = false;
                    break;
                default:
                    System.out.println("Esa opción no es válida");
                    Thread.sleep(2000);
                    clearConsole();
                    break;
            }

        }
    }

}
