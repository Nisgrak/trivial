package trivialv2;

import java.util.Scanner;
import static trivialv2.Main.questionList;
import static trivialv2.Question.generateQuestion;
import static trivialv2.Game.*;

/**
 *
 * @author Eneko Rodríguez Gonzáñez <enekorodriguezgonzalez at gmail.com>
 */
public class Config {

    private static int maxRounds;

    public static int getMaxRounds() {
        return maxRounds;
    }

    public static void setMaxRounds(int maxRounds) {
        Config.maxRounds = maxRounds;
        Database.updateMaxRounds();
    }

    public static void changeMaxRounds() {
        System.out.format("\nEl número de rondas actual es %s\n", Config.getMaxRounds());

        Config.setMaxRounds(getInt("Nuevo máximo de rondas:", 0));

    }

    /**
     * Show all question with format Number | Ask | Answer | Score
     */
    public static void showQuestions() {
        Scanner input = new Scanner(System.in);

        System.out.println("\nNúmero | Pregunta | Respuesta | Puntuación");

        for (Question question : questionList) {
            System.out.format("%d | %s | %s | %d\n", questionList.indexOf(question) + 1, question.getQuestion(), question.getAnswer(), question.getScore());
        }

    }

    /**
     * Ask user to select one question, what wanna change and new value, then load in DB
     */
    public static void selectAndChangeQuestion() {
        int numberQuestion = getInt("\nIntroduce el número de la pregunta a modificar:", 1, questionList.size());
        
        Question questionToChange = questionList.get(numberQuestion - 1);

        boolean writing = true;
        while (writing) {
            writing = false;

            String menuText = "\nQue parte quires modificar?\n\n"
                    + "1. Pregunta\n"
                    + "2. Respuesta\n"
                    + "3. Puntuación\n"
                    + "4. Eliminar pregunta\n"
                    + "5. Volver\n";

            switch (getInt(menuText)) {
                case 1:
                    questionToChange.makeQuestion(true);
                    break;
                case 2:
                    questionToChange.makeAnswer(true);
                    break;
                case 3:
                    questionToChange.makeScore(true);
                    break;
                case 4:
                    Database.deleteQuestion(questionToChange);
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Opción no válida");
                    writing = true;
                    break;
            }
        }
    }

    /**
     * Show config menu
     */
    public static void showConfig() {
        boolean writing = true;
        while (writing) {
            writing = false;
            String menuText = "\nPosibles configuraciones:\n"
                    + "1. Añadir preguntas\n"
                    + "2. Modificar preguntas\n"
                    + "3. Número máximo de rondas\n"
                    + "4. Volver\n";

            switch (getInt(menuText)) {
                case 1:
                    generateQuestion();
                    break;
                case 2:
                    showQuestions();
                    selectAndChangeQuestion();
                    break;
                case 3:
                    changeMaxRounds();
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Opción no válida");
                    writing = true;
            }
        }
    }

}
