/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trivialv2;

import java.util.Scanner;
import static trivialv2.Main.playersList;

/**
 *
 * @author eneko
 */
public class Player {

    private int Id;
    private String Name;
    private int Score = 0;

    public Player() {
    }

    public Player(int Id, String Name, int Score) {
        this.Id = Id;
        this.Name = Name;
        this.Score = Score;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int Score) {
        this.Score = Score;
    }

    public void sumScore(int ScoreToSum) {
        this.Score += ScoreToSum;
    }
    
    /**
     * Ask name for player, if input is :q then exit and return false
     * 
     * @return Player update or not
     */
    public boolean makeName() {
        Scanner input = new Scanner(System.in);
        String name = "";

        boolean writingName = true;
        while (writingName) {

            System.out.format("Nombre jugador %d:", playersList.size() + 1);
            name = input.nextLine();

            if (name.equals(":q")) {
                return false;

            } else if (name.length() > 0) {
                writingName = false;

            }
        }

        this.setName(name);
        return true;

    }

    /**
     * Generate new Player 
     * @return 
     */
    public static boolean generatePlayer() {
        Player newPlayer = new Player();
        boolean createResult = newPlayer.makeName();

        if (createResult) {
            newPlayer.setId(playersList.size() + 1);
            playersList.add(newPlayer);

        }
        return createResult;
    }

    /**
     * Return summary of number and score of player
     * @return Summary of player
     */
    public String showSummary() {
        return String.format("%s | %d puntos\n", this.getName(), this.getScore());
    }

}
