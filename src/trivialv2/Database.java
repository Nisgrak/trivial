package trivialv2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static trivialv2.Main.playersList;
import static trivialv2.Main.questionList;

/**
 *
 * @author Eneko Rodríguez Gonzáñez <enekorodriguezgonzalez at gmail.com>
 */
public class Database {

    private static final String PATH_DB = "./Database/database.db";
    private static final String POPULATE_DB = "./Database/populate.sql";
    private static final String URL_CONECTION = "jdbc:sqlite:" + PATH_DB;
    private static Connection conn;

    /**
     * Store new connection in conn variable
     * 
     * 
     */
    private static void createConnect() {
        try {
            // create a connection to the database
            conn = DriverManager.getConnection(URL_CONECTION);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }
    
    /**
     * Create database schema and make a connection
     * 
     * @throws SQLException 
     */
    private static void createDB() throws SQLException, IOException {
        boolean createNewFile = new File(PATH_DB).createNewFile();
        Database.createConnect();
        String createConfig = "CREATE TABLE Config (MaxRounds INTEGER);";

        String createPlayers = "CREATE TABLE Players (\n"
                + "  PlayerId INTEGER,\n"
                + "  Name TEXT(50) NOT NULL,\n"
                + "  Score TEXT(25) NOT NULL,\n"
                + "  PRIMARY KEY(PlayerId)\n"
                + ");";

        String createQuestions = "CREATE TABLE Questions (\n"
                + "	QuestionId INTEGER,\n"
                + "	Question TEXT NOT NULL,\n"
                + "	Answer TEXT NOT NULL,\n"
                + "	Score INTEGER NOT NULL,\n"
                + "		PRIMARY KEY(QuestionId)\n"
                + ");";

        ArrayList<String> queries = new ArrayList();
        queries.add(createConfig);
        queries.add(createPlayers);
        queries.add(createQuestions);
        makeSimpleBatchQueries(queries);
    }

    /**
     * Populate the db with default questions and config
     * 
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private static void populateDB() throws FileNotFoundException, IOException {
        Database.createConnect();
        BufferedReader bis = new BufferedReader(new FileReader(POPULATE_DB));
        ArrayList<String> queries = new ArrayList();

        String temp = null;
        while ((temp = bis.readLine()) != null) {
            
            if (!temp.equals("")){
                queries.add(temp);
                
            }
        }

        makeSimpleBatchQueries(queries);
    }

    /**
     * Make connection with database and get all needed data,
     * if the db don't exist then create and populate the db
     * 
     * @throws SQLException
     * @throws IOException 
     */
    public static void initialize() throws SQLException, IOException {
        if (!new File(PATH_DB).exists()) {
            createDB();
            populateDB();
        } else {
            Database.createConnect();
        }

        Database.getPlayers();
        Database.getQuestions();
        Database.getMaxRounds();
    }

    /**
     * Get query template based in the param
     * @param queryText Template with ? as comodin
     * @return Object template
     */
    private static PreparedStatement getTemplate(String queryText) {
        try {
            return conn.prepareStatement(queryText);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    
    /**
     * Get simple query object to batch without template
     * 
     * @return Query object
     */
    private static Statement getCleanQuery() {
        try {
            return conn.createStatement();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Execute a query in the db
     * 
     * @param queryText Query to execute
     * @return Data results
     */
    private static ResultSet makeQuery(String queryText) {
        try {
            Statement stmt = conn.createStatement();
            stmt.closeOnCompletion();
            ResultSet results = stmt.executeQuery(queryText);

            return results;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Make a single query with comodin without results
     * 
     * @param queryText Query to exec with ? comodin
     * @param args Text to insert in comodins
     */
    private static void makeTemplateQuery(String queryText, Object... args) {
        try {

            PreparedStatement pstmt = getTemplate(queryText);

            for (int i = 0; i < args.length; i++) {
                String argString = args[i].toString();

                pstmt.setString(i + 1, argString);
            }

            pstmt.executeUpdate();
//            pstmt.getConnection().close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Make single query without response
     * 
     * @param queryText Query to execute
     */
    private static void makeQueryWithoutResponse(String queryText) {
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(queryText);
//            stmt.getConnection().close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Make same query with diferentes values
     * 
     * @param queryText Query to execute
     * @param args List of lists that contain the text to insert
     */
    private static void makeTemplateBatchQuery(String queryText, ArrayList<ArrayList> args) {
        try {
            PreparedStatement pstmt = getTemplate(queryText);

            for (ArrayList element : args) {
                for (int i = 0; i < element.size(); i++) {

                    Object value = element.get(i);
                    pstmt.setString(i + 1, value.toString());
                }

                pstmt.addBatch();
            }

            pstmt.executeBatch();
//            pstmt.getConnection().close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }
    }

    /**
     * Execute queries in batch
     * 
     * @param queries List of queries to execute
     */
    private static void makeSimpleBatchQueries(ArrayList<String> queries) {
        try {
            Statement pstmt = getCleanQuery();

            for (String query : queries) {
                
                pstmt.addBatch(query);
            }

            pstmt.executeBatch();
        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }
    }

    //region CRUD Players
    /**
     * Load all Players in playersList
     */
    public static void getPlayers() {
        String queryText = "SELECT PlayerId, Name, Score FROM Players";
        playersList.clear();

        try (ResultSet rowData = makeQuery(queryText)) {

            while (rowData.next()) {
                Player player = new Player();

                player.setId(rowData.getInt("PlayerId"));
                player.setName(rowData.getString("Name"));
                player.setScore(rowData.getInt("Score"));

                playersList.add(player);
            }
            rowData.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Update scores of all players in db
     */
    public static void updateAllScores() {
        String queryText = "UPDATE Players SET Score = ? WHERE PlayerId = ?";

        ArrayList<ArrayList> listElements = new ArrayList();

        for (Player player : playersList) {
            ArrayList listValues = new ArrayList();

            listValues.add(player.getScore());
            listValues.add(player.getId());

            listElements.add(listValues);
        }

        makeTemplateBatchQuery(queryText, listElements);
    }

    /**
     * Update score in db of single players
     * 
     * @param player Player to update
     */
    public static void updateScore(Player player) {
        String queryText = "UPDATE Players SET Score = ? WHERE PlayerId = ?";

        makeTemplateQuery(queryText, player.getScore(), player.getId());
    }

    /**
     * Clear all players in db
     */
    public static void clearPlayers() {
        String queryText = "Delete from Players";

        makeQueryWithoutResponse(queryText);
    }

    /**
     * Store all players in playerList in db
     */
    public static void storePlayers() {
        String queryText = "INSERT INTO Players(PlayerId, Name, Score) VALUES(?, ?, ?)";
        clearPlayers();

        ArrayList<ArrayList> listElements = new ArrayList();

        for (Player player : playersList) {
            ArrayList listValues = new ArrayList();

            listValues.add(player.getId());
            listValues.add(player.getName());
            listValues.add(player.getScore());

            listElements.add(listValues);
        }

        makeTemplateBatchQuery(queryText, listElements);
    }

    //region CRUD Questions
    /**
     * Get all questions and store questionList
     */
    public static void getQuestions() {
        String queryText = "SELECT QuestionId, Question, Answer, Score FROM Questions";
        questionList.clear();

        try (ResultSet rowData = makeQuery(queryText)) {

            while (rowData.next()) {
                Question question = new Question();

                question.setId(rowData.getInt("QuestionId"));
                question.setQuestion(rowData.getString("Question"));
                question.setAnswer(rowData.getString("Answer"));
                question.setScore(rowData.getInt("Score"));

                questionList.add(question);
            }
            rowData.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Delete all question in db
     */
    public static void clearQuestions() {
        String queryText = "Delete from Questions";

        makeQueryWithoutResponse(queryText);
    }

    /**
     * Get all questions in db and load in questionList
     * 
     * @throws SQLException 
     */
    public static void storeQuestions() throws SQLException {
        String queryText = "INSERT INTO Questions(QuestionId, Question, Answer, Score) VALUES(?, ?, ?, ?)";
        clearQuestions();

        ArrayList<ArrayList> listElements = new ArrayList();

        for (Question question : questionList) {
            ArrayList listValues = new ArrayList();

            listValues.add(question.getId());
            listValues.add(question.getQuestion());
            listValues.add(question.getAnswer());
            listValues.add(question.getScore());

            listElements.add(listValues);
        }

        makeTemplateBatchQuery(queryText, listElements);
    }

    /**
     * Update all params in single question
     * 
     * @param question Question to update
     */
    public static void updateQuestion(Question question) {
        String queryText = "UPDATE Questions SET Question = ? ,"
                + "Answer = ? ,"
                + "Score = ?"
                + "WHERE QuestionId = ?";

        makeTemplateQuery(queryText, question.getQuestion(), question.getAnswer(), question.getScore(), question.getId());
    }

    /**
     * Delete single question, in db and in questionList
     * 
     * @param question Question to delete
     */
    public static void deleteQuestion(Question question) {
        String queryText = "DELETE from Questions WHERE QuestionId = ?";

        makeTemplateQuery(queryText, question.getId());
        questionList.remove(question);
    }

    /**
     * Create a new question, in db and in questionList
     * 
     * @param question Question to update
     */
    public static void createQuestion(Question question) {
        String queryText = "INSERT INTO Questions(QuestionId, Question, Answer, Score) VALUES(?, ?, ?, ?)";

        makeTemplateQuery(queryText, question.getId(), question.getQuestion(), question.getAnswer(), question.getScore());
        questionList.add(question);
    }

    //region CRUD Config
    /**
     * Get max rounds and load in Config.setMaxRounds
     */
    public static void getMaxRounds() {
        String queryText = "SELECT MaxRounds FROM Config";

        try (ResultSet rowData = makeQuery(queryText)) {

            while (rowData.next()) {
                Config.setMaxRounds(rowData.getInt("MaxRounds"));

            }
            rowData.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Load Config.setMaxRounds in db
     */
    public static void updateMaxRounds() {
        String queryText = "UPDATE Config SET MaxRounds = ?";

        makeTemplateQuery(queryText, Config.getMaxRounds());

    }
}
