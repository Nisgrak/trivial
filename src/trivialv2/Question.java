package trivialv2;

import java.util.Scanner;
import static trivialv2.Database.updateQuestion;
import static trivialv2.Game.getInt;
import static trivialv2.Main.questionList;

/**
 *
 * @author Eneko Rodríguez Gonzáñez <enekorodriguezgonzalez at gmail.com>
 */
public class Question {

    private int Id;
    private String Question;
    private String Answer;
    private int Score;

    public Question(int Id, String Question, String Answer, int Score) {
        this.Id = Id;
        this.Question = Question;
        this.Answer = Answer;
        this.Score = Score;
    }

    public Question() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String Question) {
        this.Question = Question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String Answer) {
        this.Answer = Answer;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int Score) {
        if (Score > 0) {
            this.Score = Score;

        } else {
            this.Score = -Score;
        }
    }

    /**
     * Ask new question for the question
     * 
     * @param updateDB Update or not the db with the change
     */
    public void makeQuestion(boolean updateDB) {
        Scanner input = new Scanner(System.in);

        System.out.println("Pregunta: ");
        this.setQuestion(input.nextLine());
        
        if (updateDB) {
            updateQuestion(this);
            System.out.println("Modificación realizada");
        }
    }

    /**
     * Ask new answer for question
     * 
     * @param updateDB Update or not the db with the change
     */
    public void makeAnswer(boolean updateDB) {
        Scanner input = new Scanner(System.in);

        System.out.println("Respuesta: ");
        this.setAnswer(input.nextLine());

        if (updateDB) {
            updateQuestion(this);
            System.out.println("Modificación realizada");
        }
    }

    /**
     * Ask new score for question
     * 
     * @param updateDB Update or not the db with the change
     */
    public void makeScore(boolean updateDB) {
        Scanner input = new Scanner(System.in);
        
        this.setScore(getInt("Puntuación :"));

        if (updateDB) {
            updateQuestion(this);
            System.out.println("Modificación realizada");
        }
    }

    /**
     * Generate new Question and add to DB
     */
    public static void generateQuestion() {
        Question newQuestion = new Question();
        newQuestion.makeQuestion(false);
        newQuestion.makeAnswer(false);
        newQuestion.makeScore(false);

        newQuestion.setId(questionList.size() + 1);
        Database.createQuestion(newQuestion);
    }

    /**
     * Make process to ask question to one user.
     * 
     * @return Answer right or not
     */
    public boolean askQuestion() {
        Scanner input = new Scanner(System.in);

        System.out.format("%s por %d puntos\n", this.getQuestion(), this.getScore());

        String userAnswer = input.nextLine();

        if (this.getAnswer().equalsIgnoreCase(userAnswer)) {
            System.out.format("Pregunta correcta, has sumado %d puntos.\n", this.getScore());
            return true;

        } else {
            System.out.println("Pregunta INCORRECTA");
            System.out.format("La respuesta correcta era %s.\n", this.getAnswer());
            return false;

        }
    }
}
